package com.ksz.customerdal.dal;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ksz.customerdal.dal.entities.Customer;
import com.ksz.customerdal.dal.repos.CustomerRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerdalApplicationTests {
	
	@Autowired
	private CustomerRepository customerrepo;
	
	@Test
	public void testCreateCustomer() {
		Customer cust = new Customer();
		cust.setName("Wojtek");
		cust.setEmail("Wojtek1234@gmail.com");
		customerrepo.save(cust);
	}
	
	@Test
	public void testCreateBunchOfCustomers() {
		Customer cust1 = new Customer();
		Customer cust2 = new Customer();
		Customer cust3 = new Customer();
		List<Customer> arr = new ArrayList<Customer>();
		cust1.setName("Marek");
		cust1.setEmail("Marek123@gmail.com");
		cust2.setName("Tomek");
		cust2.setEmail("Tomek321@gmail.com");
		cust3.setName("Rafal");
		cust3.setEmail("Rafal567@gmail.com");
		arr.add(cust1);
		arr.add(cust2);
		arr.add(cust3);
		customerrepo.saveAll(arr);
	}
	
	@Test
	public void testFindCustomerById ()
	{
		Customer c = customerrepo.findById(1L).get();
		System.out.println(c);
	}
	
	@Test
	public void testUpdateCustomer ()
	{
		Customer c = customerrepo.findById(1L).get();
		c.setEmail("maruu122@gmail.com");
	    customerrepo.save(c);
	}
	
	@Test
	public void testDeleteCustomer ()
	{
	    customerrepo.deleteById(1L);
	}

}
